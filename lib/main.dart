import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}



class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  void _doNothing() {

  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        body: SafeArea(
          child: Container(
            height: double.infinity,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: Image.network("https://image.winudf.com/v2/image1/Y29tLm1vYmlsZXVuaXZlcnNpdHkuZm9vZF9zY3JlZW5fMl8xNTQyNzE2MzUyXzA0OQ/screen-2.jpg?fakeurl=1&type=.jpg").image,
                fit: BoxFit.fitWidth,
                colorFilter: ColorFilter.mode(Colors.black38, BlendMode.multiply)
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
              Text('Welcome to Food Delivery App',
                  style: TextStyle(fontSize: 45, color: Colors.white),
                  textAlign: TextAlign.center,),
              Text('Enjoy deliveries tasty foods from any restaurant',
                style: TextStyle(fontSize: 20, color: Colors.white),
                textAlign: TextAlign.center,),
              RaisedButton( onPressed: _doNothing,
                child:  ConstrainedBox(
                  constraints: BoxConstraints.tightFor(width: 350),
                  child: Center(
                      child: Row(
                          children: <Widget>[
                            Icon(Icons.question_answer),
                            Text('Continue with phone',
                                style: TextStyle(fontSize: 20)),
                          ],
                      mainAxisAlignment: MainAxisAlignment.center,),
                  ),
                ),
                color: Colors.white,
                textColor: Colors.black,
                padding: EdgeInsets.all(10),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: BorderSide(color: Colors.white)
                ),
              ),
              Container(height: 10,),
              RaisedButton( onPressed: _doNothing,
                child: ConstrainedBox(
                  constraints: BoxConstraints.tightFor(width: 350),
                  child: Center(child: Text('Continue with phone',
                      style: TextStyle(fontSize: 20))),
                ),
                color: Colors.orange,
                textColor: Colors.white,
                padding: EdgeInsets.all(10),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: BorderSide(color: Colors.orange)
                ),
              ),
                Container(height: 10,),
              ]
            ),
          ),
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
